package me.exerosis.componentgame.factory;

import me.exerosis.componentgame.Game;

public interface GameFactory {
	public Game getNextGame();
}
