package me.exerosis.componentgame.game.lms.corpse;

import java.util.HashSet;
import java.util.Set;

import me.exerosis.componentgame.InstancePool.Depend;
import me.exerosis.componentgame.component.Component;
import me.exerosis.componentgame.component.core.player.death.spectate.SpectateComponent;
import me.exerosis.componentgame.event.game.GameStateChangeEvent;
import me.exerosis.componentgame.event.game.player.PlayerKilledEvent;
import me.exerosis.componentgame.gamestate.GameState;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class CorpseComponent extends Component{
	@Depend
	private SpectateComponent _spectateComponent;
	private Set<Corpse> corpses = new HashSet<Corpse>();

	public CorpseComponent() {}

	@Override
	public void onEnable() {
		registerListener(this);
	}

	@Override
	public void onDisable() {
		unregisterListener(this);
	}

	//Listeners
	@EventHandler
	public void onDeath(PlayerKilledEvent event){
		if(!getArena().getGameState().equals(GameState.IN_GAME, GameState.POST_GAME))
			return;
		Player player = event.getPlayer();
		new Corpse(player);
	}

	@EventHandler
	public void onGameStateChange(GameStateChangeEvent event){			
		if(event.getNewGameState().equals(GameState.RESTARTING))
			for(Corpse corpse : corpses)
				corpse.sendModCommand("RemoveTab, Remove");
	}
}
