package me.exerosis.componentgame.game.lms;

import me.exerosis.componentgame.Arena;
import me.exerosis.componentgame.InstancePool.Depend;
import me.exerosis.componentgame.component.Component;
import me.exerosis.componentgame.component.core.pause.PauseCompoent;
import me.exerosis.componentgame.event.game.post.PostGameStateChangeEvent;
import me.exerosis.componentgame.gamestate.GameState;
import me.exerosis.util.FreezePlayerUtil;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class PreGameMotionStopComponent extends Component {
	@Depend
	private PauseCompoent _pauseCompoent;
	
	public PreGameMotionStopComponent() {}

	@SuppressWarnings("static-method")
	@EventHandler
	public void onGameStateChange(PostGameStateChangeEvent event){
		if(event.getGameState().equals(GameState.IN_GAME))
			FreezePlayerUtil.getInstance().setAllFrozen(false, false);
		else if(event.getGameState().equals(GameState.PRE_GAME))
			FreezePlayerUtil.getInstance().setAllFrozen(true, false);
	}
	
	@Override
	public void run() {
		if(!_pauseCompoent.isPaused())
			for(Player player : Arena.getPlayers())
				player.setFoodLevel(20);
	}

	@Override
	public void onEnable() {
		registerListener(this);
	}

	@Override
	public void onDisable() {
		unregisterListener(this);
	}
}