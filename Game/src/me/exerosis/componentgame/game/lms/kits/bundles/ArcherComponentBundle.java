package me.exerosis.componentgame.game.lms.kits.bundles;

import java.util.LinkedList;

import me.exerosis.componentgame.component.Component;
import me.exerosis.componentgame.component.ComponentBundle;
import me.exerosis.componentgame.game.lms.kits.Archer;
import me.exerosis.componentgame.game.lms.weapons.ShortSword;

public class ArcherComponentBundle implements ComponentBundle {

	public ArcherComponentBundle() {}
	
	@Override
	public LinkedList<Component> getComponents() {
		LinkedList<Component> compoents = new LinkedList<Component>();
		compoents.add(new Archer());
		compoents.add(new ShortSword());
		//compoents.add(new LongbowBow());
		return compoents;
	}
}
