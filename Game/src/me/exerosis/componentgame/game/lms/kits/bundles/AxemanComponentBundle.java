package me.exerosis.componentgame.game.lms.kits.bundles;

import java.util.LinkedList;

import me.exerosis.componentgame.component.Component;
import me.exerosis.componentgame.component.ComponentBundle;
import me.exerosis.componentgame.game.lms.kits.Axeman;
import me.exerosis.componentgame.game.lms.weapons.BattleAxe;
import me.exerosis.componentgame.game.lms.weapons.throwing.axes.ThrowingAxe;
import me.exerosis.componentgame.game.lms.weapons.throwing.axes.ThrowingAxeCooldown;

public class AxemanComponentBundle implements ComponentBundle {

	public AxemanComponentBundle() {}
	
	@Override
	public LinkedList<Component> getComponents() {
		LinkedList<Component> compoents = new LinkedList<Component>();
		compoents.add(new Axeman());
		compoents.add(new BattleAxe());
		compoents.add(new ThrowingAxeCooldown());
		compoents.add(new ThrowingAxe());
		return compoents;
	}
}