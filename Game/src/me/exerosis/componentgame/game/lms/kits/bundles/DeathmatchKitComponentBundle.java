package me.exerosis.componentgame.game.lms.kits.bundles;

import java.util.LinkedList;

import me.exerosis.componentgame.component.Component;
import me.exerosis.componentgame.component.ComponentBundle;
import me.exerosis.componentgame.component.core.kit.KitsComponent;

public class DeathmatchKitComponentBundle implements ComponentBundle {

	public DeathmatchKitComponentBundle() {}
	
	@Override
	public LinkedList<Component> getComponents() {
		LinkedList<Component> compoents = new LinkedList<Component>();
		compoents.add(new KitsComponent());
		//compoents.addAll(new ArcherComponentBundle().getComponents());
		compoents.addAll(new AxemanComponentBundle().getComponents());
		compoents.addAll(new MageComponentBundle().getComponents());
		return compoents;
	}
}
