package me.exerosis.componentgame.game.lms.weapons.throwing.axes;

import org.bukkit.ChatColor;

import me.exerosis.componentgame.component.core.cooldown.Cooldown;

public class ThrowingAxeCooldown extends Cooldown {

	public ThrowingAxeCooldown() {}

	@Override
	public double getTime() {
		return 0.3;
	}

	@Override
	public int getSlot() {
		return 1;
	}

	@Override
	public String getCompleteMessage() {
		return ChatColor.GREEN + "You grab another axe!";
	}

	@Override
	public int getPriority() {
		return 0;
	}
}