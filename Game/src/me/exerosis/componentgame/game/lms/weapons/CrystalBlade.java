package me.exerosis.componentgame.game.lms.weapons;

import me.exerosis.componentgame.component.core.weapon.Weapon;

public class CrystalBlade extends Weapon {
	public CrystalBlade() {}

	@Override
	public String getConfigName() {
		return "crystalBlade";
	}
}