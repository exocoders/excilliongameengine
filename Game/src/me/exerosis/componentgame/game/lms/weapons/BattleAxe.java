package me.exerosis.componentgame.game.lms.weapons;

import me.exerosis.componentgame.component.core.weapon.Weapon;

public class BattleAxe extends Weapon{

	public BattleAxe() {}

	@Override
	public String getConfigName() {
		return "battleAxe";
	}
}