package me.exerosis.componentgame.game.lms.weapons;

import me.exerosis.componentgame.component.core.weapon.Weapon;

public class ShortSword extends Weapon{

	public ShortSword() {}

	@Override
	public String getConfigName() {
		return "shortSword";
	}
}