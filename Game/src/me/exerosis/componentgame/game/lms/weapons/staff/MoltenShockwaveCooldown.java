package me.exerosis.componentgame.game.lms.weapons.staff;

import org.bukkit.ChatColor;

import me.exerosis.componentgame.component.core.cooldown.Cooldown;

public class MoltenShockwaveCooldown extends Cooldown {
	@Override
	public int getSlot() {
		return 1;
	}

	@Override
	public String getCompleteMessage() {
		return ChatColor.GREEN + "Molten Shockwave Ready!";
	}

	@Override
	public double getTime() {
		return 20;
	}

	@Override
	public int getPriority() {
		return 0;
	}
}
