package me.exerosis.componentgame.game.spleef.doublejump;

import org.bukkit.ChatColor;

import me.exerosis.componentgame.component.core.cooldown.Cooldown;

public class DoubleJumpCooldown extends Cooldown {
	public DoubleJumpCooldown() {}

	@Override
	public double getTime() {
		return 30;
	}

	@Override
	public int getSlot() {
		return 0;
	}

	@Override
	public String getCompleteMessage() {
		return ChatColor.GREEN + "Double Jump Recharged";
	}

	@Override
	public int getPriority() {
		return 0;
	}
}
