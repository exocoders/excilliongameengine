package me.exerosis.componentgame.component;

import java.util.LinkedList;

public interface ComponentBundle {
	public LinkedList<Component> getComponents();
}
