package me.exerosis.componentgame.component.core.player.death.spectate;

import java.util.LinkedList;

import me.exerosis.componentgame.component.Component;
import me.exerosis.componentgame.component.ComponentBundle;

public class SpectateComponentBundle implements ComponentBundle {

	public SpectateComponentBundle() {}

	@Override
	public LinkedList<Component> getComponents() {
		LinkedList<Component> compoents = new LinkedList<Component>();
		compoents.add(new SpectateGamemode());
		compoents.add(new SpectateComponent());
		compoents.add(new SpectatorInventoryComponent());
		return compoents;
	}
}