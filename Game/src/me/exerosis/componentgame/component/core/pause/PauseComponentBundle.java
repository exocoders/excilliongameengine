package me.exerosis.componentgame.component.core.pause;

import java.util.LinkedList;

import me.exerosis.componentgame.component.Component;
import me.exerosis.componentgame.component.ComponentBundle;
import me.exerosis.componentgame.countdown.countdowns.GameResumeCountdown;

public class PauseComponentBundle implements ComponentBundle{

	public PauseComponentBundle() {}

	@Override
	public LinkedList<Component> getComponents() {
		LinkedList<Component> compoents = new LinkedList<Component>();
		compoents.add(new PauseCompoent());
		compoents.add(new GameResumeCountdown());
		return compoents;
	}
}
