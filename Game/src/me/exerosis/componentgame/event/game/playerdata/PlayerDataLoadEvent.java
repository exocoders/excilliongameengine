package me.exerosis.componentgame.event.game.playerdata;

import org.bukkit.entity.Player;

import me.exerosis.componentgame.component.core.player.PlayerData;
import me.exerosis.componentgame.event.game.GameEvent;

public class PlayerDataLoadEvent extends GameEvent{

	private PlayerData _data;
	private Player _player;

	public PlayerDataLoadEvent(Player player, PlayerData data) {
		_player = player;
		_data = data;
	}
	
	public PlayerData getData() {
		return _data;
	}
	public Player getPlayer() {
		return _player;
	}
}
