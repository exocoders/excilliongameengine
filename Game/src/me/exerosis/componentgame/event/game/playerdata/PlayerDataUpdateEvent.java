package me.exerosis.componentgame.event.game.playerdata;

import org.bukkit.entity.Player;

import me.exerosis.componentgame.event.game.GameEvent;

public class PlayerDataUpdateEvent extends GameEvent {

	private Object _data;
	private String _id;
	private Player _player;

	public PlayerDataUpdateEvent(String id, Object data, Player player) {
		_id = id;
		_data = data;
		setPlayer(player);
	}
	
	public Object getData() {
		return _data;
	}
	public String getId() {
		return _id;
	}
	public void setData(Object data) {
		_data = data;
	}
	public void setId(String id) {
		_id = id;
	}
	public Player getPlayer() {
		return _player;
	}
	public void setPlayer(Player player) {
		_player = player;
	}
}
