package me.exerosis.componentgame.event.game.cooldown;

import org.bukkit.entity.Player;

import me.exerosis.componentgame.component.core.cooldown.Cooldown;
import me.exerosis.componentgame.event.game.GameEvent;

public class CooldownFinishEvent extends GameEvent {
	private Cooldown  _cooldown;
	private Player _player;

	public CooldownFinishEvent(Cooldown cooldown, Player player) {
		_cooldown = cooldown;
		_player = player;
	}
	
	public Cooldown getCooldown() {
		return _cooldown;
	}
	public Player getPlayer() {
		return _player;
	}
}
