package me.exerosis.componentgame.event.game;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameEvent extends Event {

	public GameEvent() {}
	
	private static final HandlerList handlers = new HandlerList();
	public HandlerList getHandlers() {return handlers;}
	public static HandlerList getHandlerList() {return handlers;}
}
