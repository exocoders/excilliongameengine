package me.exerosis.componentgame;

import java.util.Arrays;
import java.util.List;

import me.exerosis.componentgame.event.game.GameStateChangeEvent;
import me.exerosis.componentgame.event.game.post.PostGameStateChangeEvent;
import me.exerosis.componentgame.factory.GameFactory;
import me.exerosis.componentgame.gamestate.GameState;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Arena {
	// Sudo game fields.
	private GameState _gameState;

	// Arena fields.
	private Game _currentGame;
	private GameFactory _factory;
	private Plugin _plugin;

	public Arena(Plugin plugin, GameFactory factory) {
		_factory = factory;
		_plugin = plugin;
	}

	// Game management.
	public void nextGame() {
		//Ensure
		if(_gameState != GameState.STARTING)
			_gameState = GameState.RESTARTING;

		// Get next.
		this._currentGame = getFactory().getNextGame();

		// Load
		_currentGame.getInstancePool().add(_currentGame);
		_currentGame.getInstancePool().add(_plugin);
		_currentGame.getInstancePool().add(this);
		_currentGame.enableGame();
	}

	public void start() {
		_gameState = GameState.STARTING;
		nextGame();
	}

	@SuppressWarnings("deprecation")
	public static List<Player> getPlayers() {
		return Arrays.asList(Bukkit.getOnlinePlayers());
	}

	// Getters and setters.
	public void setGameState(GameState gameState) {
		Bukkit.getPluginManager().callEvent(new GameStateChangeEvent(_gameState, gameState));
		this._gameState = gameState;
		Bukkit.getPluginManager().callEvent(new PostGameStateChangeEvent(gameState));
	}

	public GameState getGameState() {
		return _gameState;
	}

	public Game getCurrentGame() {
		return _currentGame;
	}

	public Plugin getPlugin() {
		return _plugin;
	}

	public GameFactory getFactory() {
		return _factory;
	}
}
