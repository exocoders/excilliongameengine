package me.exerosis;

import me.exerosis.componentgame.Arena;
import me.exerosis.componentgame.factory.RotatingGame;
import me.exerosis.componentgame.game.lms.DeathmatchGame;
import me.exerosis.util.FreezePlayerUtil;
import net.minecraft.server.v1_8_R1.Entity;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{

	@Override
	public void onEnable() {
		new Arena(this, new RotatingGame(new DeathmatchGame())).start();
		FreezePlayerUtil.getInstance().setPlugin(this);
		Bukkit.getPluginManager().registerEvents(this, this);
	}
    public static void spawnCustomMob(Location location, Entity entity){
    	entity.setPosition(location.getX(), location.getY(), location.getZ());
        ((CraftWorld)location.getWorld()).getHandle().addEntity(entity);
    }
	@SuppressWarnings("static-method")
	@EventHandler(priority = EventPriority.MONITOR)
	public void onTest(EntityShootBowEvent event){	
		System.out.println("event");
		//new Ground(event.getPlayer().getLocation(), 20);
		//player.sendPacket(ScoreboardUtil.getUpdateScorePacket("Test", 1));
	
		
		//Player player = event.getPlayer();	
		/*
			for(PacketPlayer player : PlayerHandler.getOnlinePlayers()) {

			new EntityEquipment(getEID()[0], 3, _playerInventory.getHelmet()).send(player);
			new EntityEquipment(getEID()[0], 2, _playerInventory.getChestplate()).send(player);
			new EntityEquipment(getEID()[0], 1, _playerInventory.getLeggings()).send(player);
			new EntityEquipment(getEID()[0], 0, _playerInventory.getBoots()).send(player);
		}
		 */
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!(sender instanceof Player))
			return false;
		if(!label.equals("test"))
			return false;
		Player player = (Player) sender;
		new CopyOfProjectileLineEffect(player.getLocation());
		return super.onCommand(sender, command, label, args);
	}
}